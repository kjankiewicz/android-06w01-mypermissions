package com.example.kjankiewicz.android_06w01_mypermissions

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class SecureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secure)
    }

}
