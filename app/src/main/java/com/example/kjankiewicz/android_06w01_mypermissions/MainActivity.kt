package com.example.kjankiewicz.android_06w01_mypermissions

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    private fun openWebPage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission("android.permission.READ_CONTACTS") !=
                PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf("android.permission.READ_CONTACTS"),
                    PERMISSIONS_REQUEST_READ_CALL_LOG)
        } else
        {
            var myWebSite: String? = null

            val selection = (ContactsContract.CommonDataKinds.Website.DISPLAY_NAME +
                    " like '%Krzysztof Jankiewicz%' " +
                    " and " + ContactsContract.Data.MIMETYPE
                    + " = '" + ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE + "'")

            val projection = arrayOf(ContactsContract.CommonDataKinds.Website.URL)
            val c = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                    projection, selection, null, null)
            if (c != null) {
                if (c.moveToFirst()) {
                    myWebSite = c.getString(0)
                } else {
                    Toast.makeText(applicationContext,
                            "There is no such website!", Toast.LENGTH_LONG).show()
                }
                c.close()
            }

            if (myWebSite != null) {
                val myWebSiteURI = Uri.parse("http://$myWebSite")
                var implicitIntent = Intent(Intent.ACTION_VIEW, myWebSiteURI)
                if (implicitIntent.resolveActivity(packageManager) != null) {
                    implicitIntent = Intent.createChooser(implicitIntent,
                            "Otwórz stronę za pomocą:")
                    startActivity(implicitIntent)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CALL_LOG) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openWebPage()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, your contacts cannot be accessed",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button = findViewById<Button>(R.id.my_page_from_contacts_button)

        button.setOnClickListener { openWebPage() }

        button = findViewById(R.id.run_secure_activity_button)

        button.setOnClickListener {
            val explicitIntent = Intent(applicationContext, SecureActivity::class.java)
            startActivity(explicitIntent)
        }
    }

    companion object {
        const val PERMISSIONS_REQUEST_READ_CALL_LOG = 1
    }

}
